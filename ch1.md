# What the redox api is composed of. 

## Syscall

The syscall interface of Redox is similar to unix with quite a few exceptions. Syscalls like open, create, read, write are included, as well as many, many common syscalls found in unix. You can learn more about these syscalls in the `redox_syscall` crate on crates.io

### Where are the syscalls come from.


The syscalls are defined in multiple places, but implemented in only 1. In the kernel, there are two directories named syscall. One of them contains kernel-side definitions of syscalls, and syscall handling, and the other contains an external API for rust programs to call redox syscalls directly. 

A great place to start reading about syscalls is in src/syscall/mod.rs. This file contains the syscall handling functions, and has a great itroduction to some lower-level redox code.

## Schemes

This is a new concept that Redox-os uses. Schemes are basically urls of the form schemename:/blah/blah/blah. These schemes are managed by the kernel, but created and run in userspace. You utilize these schemes using normal syscalls. In this way, redox edits the time-old unix slogan of "everything is a file", and replaces it with "everything is a URL". Lets look at this more by examining some schemes found on redox.

1. the `file:` scheme: This scheme is for accessing files on the disks. `cd file:` is the same as `cd /`, and open("file:/etc/passwd") is the same as open("/etc/passwd").
2. the `zero:` scheme: Linux's equivilent of /dev/zero. Read from this to fill any buffers with plenty of zeroes. Discards all writes.
