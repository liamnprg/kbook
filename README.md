# Kbook

A book about the redox kernel, made with mdbook




# License

The license is in LICENSE and [This link](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![Attribution-NonCommercial-ShareAlike](./by-nc-sa.png)
