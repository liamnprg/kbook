# What is the redox kernel(In a little bit more detail than usual)

## Architecture

The architecture of redox-os is microkernel. We chose microkernel because of MMSS. 


### Modularity

Because little is set in stone, the kernel api can change quite easily based on what users(or their programs) want.

### Maintainability

15 thousand lines vs well more than 20 million lines of code. Which is easier to maintain. Yeah, I agree. 

### Security

Surface reduction is one of the most valuable ways to improve application security. Microkernels take this logic to the extreme. When all of the redox-os api runs in userspace, its hard for one compromized driver to ruin the security of the whole system. 

### Stability

Kernel code REQUIRES stability. Dealing with errors generated in the kernel can be tough, and can even lead to page faults, errors, segfaults and worse. 


## The redox-os API

The redox api is composed of multiple entrypoints into the kernel. The gateway for these entrypoints are syscalls. Everything is done through syscalls. 




