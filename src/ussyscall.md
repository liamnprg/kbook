# Syscalls from the user's perspective.

This chapter will go over the [redox\_syscall](https://crates.io/crates/redox_syscall) crate in more detail. 

## What this crate does.

This crate has 7 functions that make a syscall of x number with y arguments. these are called `syscall0`,`syscall1`,`syscall1_clobber`,`syscall2` ... `syscall5`. `syscall0` is for a syscall that takes zero arguments, `syscall1` is for syscalls that take 1 argument, and `syscall1_clobber` is for syscalls that take 1 argument and need the cpu registers cleared out before execution.

Using these 7 functions, every syscall that Redox supports is represented. These include `open`, `create`, `clone`, ect. This allows for easy access to the Redox syscall interface.

## Other things this crate does.

It defines traits for crating schemes, as well as providing names for error numbers and syscalls.
