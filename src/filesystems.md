# Filesystems

Filesystems are responsible for taking the ability to read/write bits to a hard drive, and turn that into the ability to store data in files and directories. 

## What RedoxFS can do right now.

RedoxFS is a full unix filesystem implementation supporting permissions, files, folders, and everything else a bare-bones unix implementation should support. 


## What RedoxFS can't do right now.

Compression, encryption, defragment, recovery, journaling.
