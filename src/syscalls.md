# Syscalls

Syscalls are the official separator between userspace and kernel space as defined by the x86 architecture. This way of thinking is so ubiquitous, even other architectures such as arm have followed suit.


## What syscalls redox provides

Redox provides a unix style syscall interface. 

## What syscalls are new?

Multiple syscalls related to schemes are new.

## Where can I find them.

You can find syscalls in [The redox-syscall crate's documentation](https://docs.rs/redox_syscall/0.1.40/syscall/call/index.html)
