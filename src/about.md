# About this book.

In the old days knowledge was passed down from generation to generation through speech. In the old days of Redox, the same could be said. This is an attempt to codify the knowledge that was passed down to me through forums and my own minimal experience. This book should help you understand the decisions we made and why we made them. The book will first give a brief overview of RedoxOs and its many new concepts. Then we will take a deep dive into kernel code.

## I was there with you until you said kernel code

Don't worry, the Redox kernel's code is surprisingly clean and easy to understand. This is a great project to get into if this is your first attempt at the whole operating systems thing. This is where I started, and am currently starting now. 

## Who wrote this

Liam Naddell or liamnprg

## Where are we

We are on a self-hosted gitlab instance on [here](gitlab.redox-os.org), and a self-hosted mattermost instance at [here](chat.redox-os.org)

## License

This work is licenced under CC-BY-NC-SA as layed out in the LICENSE file and README.md.

## Enough talking lets get into it

lets go!!!!
