# FAQ

## What _is_ Redox?

Redox is a unix-like microkernel written in rust.

## Why microkernel? I thought those died after GNU hurd.

Many people(including myself) thought this way before seeing a successful microkernel design. GNU Hurd was unnecessarily complicated, something good microkernels should strive to avoid. The definition of micro kernel should be pushing code from kernel-space to user-space. What part of that brings in complication?  All of this begs the question: Why are did one failed kernel ruin microkernels for a whole generation? It's simply because kernels don't crop up that often. Its not like re-implementing `ls` or something, kernels take time, lots of it. So when a kernel fails, people often don't have any idea of what success looks like for a particular model of code.

### The three largest operating systems are macrokernels, why is that if microkernels are the best way forward?

That is a great question, and I think I have the answer: Macrokernels do not provide as much modularity. This means that companies that want to control the hardware their software is deployed to will often write macrokernels for economic incentive. All userspace components can be stripped away, so the more that is in the kernel, the less that can be taken away, and the more control the software has over the hardware it's deployed to.
 

## What does the Redox kernel do on a high-level?

The Redox kernel is responsible for facilitating access to hardware, as well as to enforce unix-style permissions.


## What filesystems does Redox use?

There are two filesystems for Redox.

1. RedoxFS - Redoxfs is Redox's version of ext4. It does not do much, but it does its job well.
2. TFS - A more zfs-like approach to filesystems. While I would love to tell you all of the awesome stuff TFS does right now, I can't because it has fallen into the pit of unmaintained software. It doesn't even compile anymore because it relies on outdated dependencies :( .

## What are some notable features that the kernel has right now?


## What are the plans for the future?


