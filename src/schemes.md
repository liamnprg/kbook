# Schemes

Schemes are one of the ways the kernel exposes functionality to users, such as files, network interfaces, null devices and that one brainf driver somebody wrote. 

## Using schemes

If you want to view the `/etc/hosts` file using your favourite editor, you could type `vi /etc/hosts`, or you could also type `vi file:/etc/hosts`. This is the `file:` scheme, and it is Redox's VFS. If you want to learn more about VFS's you should check out
[wikipedia](https://en.wikipedia.org/wiki/Virtual_file_system) and [osdev](https://wiki.osdev.org/VFS)

Another example of the scheme is the `zero:` scheme. This will discard all writes, and will fill any buffer sent to it with null bytes. 

## How do you interact with schemes?

Using unix-style [syscalls](https://en.wikipedia.org/wiki/System_call) of course! 


## How are schemes defined?

There are multiple places schemes are defined. Some are defined in the kernel itself such as `initfs:` and `root:`(both of which we will talk about later), and most are defined in userspace, `null:`, `zero:`.


### Kernel schemes

Kernel schemes are defined inside the kernel source code, and I can't describe the proccess until later chapters. Please see the chapter on Schemes in depth.


### Userspace schemes

Userspace schemes are defined more transparently. If we use the redox\_syscall crate, then we can easily\* create new schemes. It is common in packages to see scheme.rs files that contain the definitions of the scheme.

The way userspace schemes are defined using redox\_syscall is with two traits, Scheme, and SchemeMut. These traits have a function containing what to do when a syscall(such as open) is called on the scheme. 
