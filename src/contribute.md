# I want to help redox out, but I don't know where to begin

It really depends on what you want to help with. If you want to help with the kernel, a great place to start is adding documentation to syscalls(its where I am starting). If you want to help with filesystems, a great place to start is reading about hard disks in redoxfs. If you want to help with userspace utilities, please look into the coreutils and the uutils repositories.

